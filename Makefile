.PHONY: start stop enter listen

start:
	docker-compose up --force-recreate -d

stop:
	docker-compose down

enter:
	docker-compose exec client bash

listen:
	sudo tcpdump -i dnsdemo udp port 53
