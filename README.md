# Self-contained DNS demo setup

This repository contains a simple, but complete demo setup for studying (recursive) DNS queries by example. It contains a DNS root server, an authoritative top-level name server (for the domain `com.`, an authoritative second-level name server (for the domain `example.com.`, as well as a caching recursive resolver. Additionally, the setup spawns a "client" container, which you may enter to issue DNS queries using `dig`.

## Prerequisites

This setup requires the following software (with versions tested by me in brackets, but other versions should also work)
- docker (20.10.3)
- docker-compose (1.28.4)
- make (GNU make 4.3)
- tcpdump (4.99.0)

The setup has only been tested on Linux. It assumes that docker is available without using `sudo`, i.e. that the user is in the `docker` group.

## The setup

The setup will start up the following containers:
- `root`: the root DNS server ([zone file](root/root.zone))
- `com`: the authoritative nameserver for the TLD `com.` ([zone file](com/com.zone))
- `example-com`: the authoritative nameserver for the domain `example.com` ([zone file](example.com/example.com.zone))
- `recursive`: a recursive resolver, having the `root` container in its root server hints file
- `client`: an "empty" container from which you can run DNS queries to the self-contained DNS setup. It is configured to use the `recursive` container as its DNS resolver.

## Running the setup

You can start up the setup with
```bash
make start
```
Once the setup is running, you can enter the `client` container by running
```bash
make enter
```
Inside the container, you can then issue some queries using `dig`:
```bash
dig example.com
dig foo.example.com
dig example.com @10.0.0.2 # directly asking the root server for a second level domain
```
From another shell, you may then also inspect the DNS traffic between the containers by running
```
make listen
```
This will ask you for sudo credentials, as the underlying `tcpdump` command needs root privileges.

## Notes

- The nameservers are configured to set the record TTL to 60 seconds. This way, you can for example observe the impact of DNS caching using `make listen` and issuing multiple queries over a certain timespan.
- The setup does not contain DNSSEC configuration at the moment
- The setup does not provide a reverse DNS configuration, yet
